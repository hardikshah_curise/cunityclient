﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CUnity.Shared.Models
{
    public class ChangePasswordModel
    {
        public string UserName { get; set; }

        [Required]
        public string CurrentPassword { get; set; }

        [Required]
        [Display(Name = "Password")]
        public string NewPassword { get; set; }

        [Required]
        [Display(Name = "New Password")]
        [Compare("NewPassword")]
        public string ConfirmNewPassword { get; set; }
    }
}
