﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CUnity.Shared.Models
{
    public class ProductRoleMapping : BaseEntity
    {
        public int RoleId { get; set; }
        public int ProductId { get; set; }
        public int ReportId { get; set; }
        public DateTime CreatedDate { get; set; }
    }
    public class ProductRoleMappingView
    { 
        public string RoleName { get; set; }
        public List<ProductRoleMappingList> lstProductRoleMapping { get; set; }
    }
    public class ProductRoleMappingList
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public int ReportId { get; set; }
        public string ReportName { get; set; }
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public bool IsAssigned { get; set; }
    }
}
