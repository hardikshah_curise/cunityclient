﻿using CUnity.Server.CommonFunctions;
using CUnity.Server.Data;
using CUnity.Server.Models;
using CUnity.Shared.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CUnity.Server.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ApplicationDBContext _context;
        public IConfiguration Configuration { get; }

        public UsersController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, RoleManager<IdentityRole> roleManager, ApplicationDBContext context,
            IConfiguration configuration)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            this._context = context;
            Configuration = configuration;
        }
        //api/users/GetUsers
        [HttpGet]
        public async Task<IActionResult> GetUsers()
        {
            var userList = _userManager.Users.AsQueryable();
            var userDtoList = new List<UserViewModel>(); // This sucks, but Select isn't async happy, and the passing into a 'ProcessEventAsync' is another level of misdirection
            foreach (var applicationUser in userList.ToList())
            {
                userDtoList.Add(new UserViewModel
                {
                    FirstName = applicationUser.FirstName,
                    LastName = applicationUser.LastName,
                    UserName = applicationUser.UserName,
                    Email = applicationUser.Email,
                    UserGuId = applicationUser.Id,
                    UserID = applicationUser.UserId,
                    Roles = await _userManager.GetRolesAsync(applicationUser).ConfigureAwait(true) as List<string>
                });
            }
            return Ok(userDtoList);
        }

        [HttpGet]
        public async Task<IActionResult> GetUserByUserID(string userID)
        {
            var user = _userManager.Users.Where(u => u.UserId == Convert.ToInt32(userID)).First();

            var client = (from a in _context.Clients
                          where a.Id == user.ClientId
                          select new CUnity.Shared.Models.ClientListView
                          {
                              Name = a.Name
                          }).ToList();

            string clientName = string.Empty;
            if (client.Count() > 0)
                clientName = client[0].Name;

            var userDto = new UserViewModel
            {
                FirstName = user.FirstName,
                LastName = user.LastName,
                UserName = user.UserName,
                Email = user.Email,
                UserGuId = user.Id,
                ClientId = user.ClientId,
                ClientName = clientName,
                Roles = await _userManager.GetRolesAsync(user).ConfigureAwait(true) as List<string>
            };
            return Ok(userDto);
        }


        //api/users/GetRoles
        [HttpGet]
        public async Task<IActionResult> GetRoles()
        {
            // var roleList = await  _roleManager.Roles.ToListAsync();

            var res = await _context.Roles.ToListAsync();
            //var context = new ApplicationDBContext();
            return Ok(res);
        }

        [HttpPost]
        public async Task<IActionResult> SaveRole(IdentityRole role)
        {
            IdentityResult res;
            role.NormalizedName = role.Name.ToUpper();
            try
            {
                if (role.Id == "0")
                {
                    role.Id = _roleManager.Roles.ToList().Count > 0 ? (Convert.ToInt32(_roleManager.Roles.ToList().Max(p => p.Id)) + 1).ToString() : "1";  // Set Custom RoleId
                    role.ConcurrencyStamp = DateTime.Now.ToString("yyyy-MM-dd");
                    res = await _roleManager.CreateAsync(role);
                }
                else
                {
                    res = await _roleManager.UpdateAsync(role);
                }
            }
            catch (DbUpdateConcurrencyException ex)
            {
                res = null;
            }

            return Ok(res);
        }

        [HttpPost]
        public async Task<IActionResult> DeleteRole(IdentityRole role)
        {
            _context.Roles.Remove(role);
            await _context.SaveChangesAsync();
            return NoContent();
        }

        [HttpPost]
        public async Task<IActionResult> ChangePassword(ChangePasswordModel changePasswordModel)
        {
            var user = await _userManager.FindByNameAsync(changePasswordModel.UserName);
            if (user == null) return BadRequest("User does not exist");

            ApplicationUser userOld = (ApplicationUser)user.Clone();

            var result = await _userManager.ChangePasswordAsync(user, changePasswordModel.CurrentPassword, changePasswordModel.NewPassword);
            if (!result.Succeeded)
                return Ok("Password updation failed");
            else
            {
                user.PasswordHash = changePasswordModel.NewPassword;
                List<UserChanges> lstuserChanges = new List<UserChanges>();
                lstuserChanges = TrackUserChanges.TrackChanges(userOld, user);
                foreach (UserChanges item in lstuserChanges)
                {
                    await _context.UserChanges.AddAsync(item);
                }
                if (lstuserChanges.Count() > 0)
                    await _context.SaveChangesAsync();
                return Ok();
            }
        }

        [HttpPost]
        public async Task<IActionResult> UpdateUserProfile(UserViewModel userViewModel)
        {
            var user = await _userManager.FindByEmailAsync(userViewModel.Email);
            if (user == null)
                return BadRequest("User does not exist");

            ApplicationUser userOld = (ApplicationUser)user.Clone();

            user.UserName = userViewModel.UserName;
            user.FirstName = userViewModel.FirstName;
            user.LastName = userViewModel.LastName;

            var result = await _userManager.UpdateAsync(user);

            if (result.Succeeded)
            {
                List<UserChanges> lstuserChanges = new List<UserChanges>();
                lstuserChanges = TrackUserChanges.TrackChanges(userOld, user);
                foreach (UserChanges item in lstuserChanges)
                {
                    await _context.UserChanges.AddAsync(item);
                }
                if (lstuserChanges.Count() > 0)
                    await _context.SaveChangesAsync();
                return Ok("User Profile updated successfully.");
            }
            else
                return Ok("Profile updation failed.");
        }

        [HttpPost]
        public async Task<IActionResult> UpdateUser(UserViewModel userViewModel)
        {
            var user = await _userManager.FindByIdAsync(userViewModel.UserGuId);
            ApplicationUser userOld = (ApplicationUser)user.Clone();
            try
            {
                user.UserName = userViewModel.UserName;
                user.Email = userViewModel.Email;
                user.FirstName = userViewModel.FirstName;
                user.LastName = userViewModel.LastName;
                user.UserId = userViewModel.UserID;
                user.ClientId = userViewModel.ClientId;
                var result = await _userManager.UpdateAsync(user);

                var userRoles = await _userManager.GetRolesAsync(user);
                await _userManager.RemoveFromRolesAsync(user, userRoles);

                List<UserChanges> lstuserChanges = new List<UserChanges>();
                lstuserChanges = TrackUserChanges.TrackChanges(userOld, user);
                foreach (UserChanges item in lstuserChanges)
                {
                    await _context.UserChanges.AddAsync(item);
                }
                if (lstuserChanges.Count() > 0)
                    await _context.SaveChangesAsync();

                foreach (RolesByUserIDModel item in userViewModel.rolesByUserIDModels)
                {
                    if (item.IsAssigned)
                    {
                        var res = await _userManager.AddToRoleAsync(user, item.RoleName);
                    }
                }
                return Ok("success");
            }
            catch (Exception ex)
            {
                return Ok("failed");
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetRolesByUserID(string userID)
        {
            DataHelper ds = new DataHelper(Configuration.GetConnectionString("DefaultConnection"));
            Microsoft.Data.SqlClient.SqlParameter[] para = new Microsoft.Data.SqlClient.SqlParameter[1];
            para[0] = new Microsoft.Data.SqlClient.SqlParameter("@UserID", Convert.ToInt32(userID));
            var listRolesByUserID = ds.ExecuteStoredProcedure<RolesByUserIDModel>("proc_GetRolesByUserID", para);

            return Ok(listRolesByUserID);
        }
    }
}
