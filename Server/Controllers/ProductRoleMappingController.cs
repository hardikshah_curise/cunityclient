﻿using CUnity.Server.Data;
using CUnity.Shared.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Identity;

namespace CUnity.Server.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ProductRoleMappingController : ControllerBase
    {
        private readonly ApplicationDBContext _context;
        public IConfiguration Configuration { get; }
        public ProductRoleMappingController(ApplicationDBContext context, IConfiguration configuration)
        {
            this._context = context;
            Configuration = configuration;
        }

        [HttpGet]
        public async Task<IActionResult> GetProductRoleMappingByRole(int roleID)
        {
            //List<ProductRoleMappingView> listProductRoleMappingView;
            //string sql = "exec [dbo].[proc_GetProductRoleMappingData] " + roleID.ToString();
            //var listProductRoleMappingView = _context.Set<ProductRoleMappingView>().FromSqlRaw("exec [dbo].[proc_GetProductRoleMappingData] " + roleID.ToString()).ToList();

            DataHelper ds = new DataHelper(Configuration.GetConnectionString("DefaultConnection"));
            Microsoft.Data.SqlClient.SqlParameter[] para = new Microsoft.Data.SqlClient.SqlParameter[1];
            para[0] = new Microsoft.Data.SqlClient.SqlParameter("@RoleID", roleID);
            var listProductRoleMappingView = ds.ExecuteStoredProcedure<ProductRoleMappingList>("proc_GetProductRoleMappingData", para);

            return Ok(listProductRoleMappingView);
        }

        [HttpPost]
        public async Task<IActionResult> SaveProductRoleMappingByRole(List<ProductRoleMappingList> lstProductRoleMapping)
        {
            _context.ProductRoleMapping.RemoveRange(_context.ProductRoleMapping.Where(x => x.RoleId == lstProductRoleMapping[0].RoleId));

            foreach (var ProductRoleMappingList in lstProductRoleMapping)
            {
                if (ProductRoleMappingList.IsAssigned)
                {
                    ProductRoleMapping prm = new ProductRoleMapping();
                    prm.RoleId = ProductRoleMappingList.RoleId;
                    prm.ProductId = ProductRoleMappingList.ProductId;
                    prm.ReportId = ProductRoleMappingList.ReportId;
                    prm.CreatedDate = System.DateTime.Now;

                    _context.ProductRoleMapping.Add(prm);
                }
            }
            await _context.SaveChangesAsync();
            return Ok();
        }
    }
}
